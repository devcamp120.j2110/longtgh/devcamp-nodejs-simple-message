const express = require('express');
const app = express();
const port = 3000;

app.get('/', (request, response)=> {
    let today = new Date();
    //response.send(`Xin chào, hôm nay là ngày ${today.getDate()}`);
    response.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()}`
    })
})

app.listen(port, ()=>{
    console.log("App listening on port: " + port);
})